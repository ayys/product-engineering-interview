from sqlalchemy import create_engine, Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from flask import Flask
from flask_restful import Resource, Api

Base = declarative_base()

class Link(Base):
    __tablename__ = 'app_sdk'
    app_id = Column(
        'app_id',
        Integer,
        ForeignKey('app.id'),
        primary_key=True)

    sdk_id = Column(
        'sdk_id',
        Integer,
        ForeignKey('sdk.id'),
        primary_key=True)

    installed = Column('installed', Boolean)




class App(Base):
    __tablename__ = "app"
    id = Column('id', Integer, primary_key=True)
    name = Column('name', String)


class SDK(Base):
    __tablename__ = "sdk"
    id = Column('id', Integer, primary_key=True)
    name = Column('name', String)


engine = create_engine('sqlite:///data.db')

Session = sessionmaker(bind=engine)

session = Session()


sdks = [_ for _ in session.query(SDK).all()]

data = session.query(App, SDK, Link).filter(
    Link.app_id == App.id,
    Link.sdk_id == SDK.id).order_by(Link.app_id).all()


                
session.close()

store = {sdk.id: {fr.id: 0 for fr in sdks} for sdk in sdks}
for from_sdk in sdks:
    for to_sdk in sdks:
        apps = [row for row in data if ((row.SDK.id == from_sdk.id and row.Link.installed is False) or (row.SDK.id == to_sdk.id and row.Link.installed is True))]
        store[from_sdk.id][to_sdk.id] = len(apps)

app = Flask(__name__)
api = Api(app)

class SDKs(Resource):
    def get(self):
        return [{"id": sdk.id, "name": sdk.name} for sdk in sdks]

class Store(Resource):
    def get(self):
        return store

    
api.add_resource(SDKs, '/')
api.add_resource(Store, '/store')

if __name__ == "__main__":
    app.run()
