function get_sdk(id) {
    return sdks.filter((row)=> row.id==id)[0]
}


async function get(url){
    const response = await fetch(url);
    var data = await response.json();
    return data
}
let sdks = []
let store = null
get("http://localhost:5000/").then(data => {
    sdks = data
    document.getElementById("sdks").innerHTML = showsdk(sdks);
})
get("http://localhost:5000/store").then(data => {
    store = data
    show_store(store)
})
function showsdk(data) {
    // Loop to access all rows
    let tab = ``
    for (let r of data) {
        tab += `<tr>
<td>${r.id} </td>
    <td>${r.name} </td>
</tr>`
        tab += ``;
    }
    // Setting innerHTML as tab variable
    return tab
}

function show_store(data, percentage) {
    let tab = ``
    for (let r in data) {
        tab += `<tr>`
        tab += `<td>${ get_sdk(r).name} </td>`
        console.log(store[r])
        let total = 0
        if (percentage) {
            for (let c in store[r]) {
                total += store[r][c]
            }
        }
        for (let c in store[r]) {
            if (percentage) {
                tab += `<td class="py-3 rows">${Math.round((store[r][c]/total)*100)}% </td>`
            } else {
            tab += `<td class="py-3 rows">${store[r][c]} </td>`
            }

        }
        tab += `</tr>`
    }
    document.getElementById("from").innerHTML = tab
    tab = `<th></th>`
    for (let r in data) {
        tab += `<th>${get_sdk(r).name} </th>`
    }
    document.getElementById("to").innerHTML = tab

}
